
#include <string>
#include <Eigen/Dense>

#include <pinocchio/multibody/model.hpp>
#include <pinocchio/multibody/data.hpp>
#include <pinocchio/parsers/urdf.hpp>
#include <pinocchio/spatial/explog.hpp>
#include <pinocchio/algorithm/kinematics.hpp>
#include <pinocchio/algorithm/jacobian.hpp>
#include <pinocchio/fwd.hpp>
#include <pinocchio/algorithm/compute-all-terms.hpp>
#include "pinocchio/algorithm/joint-configuration.hpp"
#include <pinocchio/math/rpy.hpp>



/************* VERY IMPORTANT: DO NOT ADD ROS BEFORE PINOCCHIO *****************/
#include <ros/ros.h>


class KinDyn {

	public:
		KinDyn(ros::NodeHandle& nodeHandle);
		void init(std::string urdf_file_name) ;
		void update() ;
		bool readParameters() ;

	private:

		pinocchio::Model model_ ;
		pinocchio::Data  data_ ;

		ros::NodeHandle& node_handle_;

		Eigen::VectorXd fbk_task_pos_ ;
		Eigen::VectorXd fbk_task_vel_ ;

		Eigen::VectorXd joint_pos_ ;
		Eigen::VectorXd joint_vel_ ;

		Eigen::MatrixXd jacobian_ ;
		Eigen::MatrixXd jacobian_dot_ ;


		Eigen::MatrixXd M_ ;
		Eigen::VectorXd h_ ;

		std::string urdf_file_name_ ;
		int dim_joints_ = 7 ;
		int hand_id_ ;
};
