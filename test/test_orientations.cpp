
#include <iostream>
#include <pinocchio/multibody/model.hpp>
#include <pinocchio/multibody/data.hpp>
#include <pinocchio/parsers/urdf.hpp>
#include <pinocchio/algorithm/kinematics.hpp>
#include <pinocchio/algorithm/jacobian.hpp>
#include <pinocchio/algorithm/joint-configuration.hpp>
#include <pinocchio/algorithm/compute-all-terms.hpp>
#include <pinocchio/algorithm/frames.hpp>

#include <ros/ros.h>


void pinocchio_to_other_datatype(pinocchio::SE3& pose) {

	Eigen::Matrix3d rotation = pose.rotation()  ; 		// rotation matrix is an Eigen::Matrix3d object
	Eigen::Quaterniond quaternion ; 					// quaternion

	pinocchio::quaternion::assignQuaternion(quaternion, rotation );	// convert rotation to quaternion
	rotation = quaternion.toRotationMatrix(); 						// convert quaternion to rotation

	ROS_INFO_STREAM("rotation: " << pose.rotation() ) ;
	ROS_INFO_STREAM("quaternion x: " << quaternion.x() << ", y: " << quaternion.y() << ", z: " << quaternion.z() << ", w: " << quaternion.w() ) ;
	ROS_INFO_STREAM("rotation " << rotation ) ;


	pinocchio::quaternion::assignQuaternion(quaternion, Eigen::Matrix3d::Identity()) ;	// convert from rotation to quaternion

	ROS_INFO_STREAM("quaternion x: " << quaternion.x() << ", y: " << quaternion.y() << ", z: " << quaternion.z() << ", w: " << quaternion.w() ) ;
	ROS_INFO_STREAM("rotation  " << quaternion.toRotationMatrix()) ; // convert from quaternion to rotation
}

void difference_between_two_pose(pinocchio::SE3& pose_fbk, pinocchio::SE3& pose_ref) {

	// calculating pose error from homogeneous transformation matrix
	pinocchio::SE3 pose_err_	   = pose_fbk.inverse() * pose_ref ; 					// difference in T
	Eigen::VectorXd err_pos_local  = pinocchio::log6(pose_err_).toVector() ;			// convert it to a vector in local frame
	Eigen::VectorXd err_pos_world  = pose_fbk.rotation() * err_pos_local.tail<3>() ; 	// convert it to a vector in the world frame

	ROS_INFO_STREAM("orientation error from transformation " << err_pos_world.transpose() ) ;

	// calculating orientation error from rotation matrix
	Eigen::Matrix3d rot_err_ 	  = pose_fbk.rotation().transpose() * pose_ref.rotation() ; // difference in rotation
	Eigen::Vector3d ang_vel_local = pinocchio::log3(rot_err_) ; 							// convert it to a vector in local frame
	Eigen::Vector3d ang_vel_world = pose_fbk.rotation() * ang_vel_local ; 					// convert it to the world frame
	ROS_INFO_STREAM("orientation error from pinocchio rotation " << ang_vel_world.transpose() ) ;

	// another way
	rot_err_ = pose_ref.rotation() * pose_fbk.rotation().transpose() ;
    Eigen::AngleAxisd angleAxis(rot_err_);
    double angle = angleAxis.angle();                 // Rotation angle in radians
    Eigen::Vector3d axis = angleAxis.axis();          // Rotation axis (unit vector)
	ROS_INFO_STREAM("orientation error calculated from Eigen" << angle * angleAxis.axis().transpose() ) ;


	ROS_INFO_STREAM("\n\n") ;
}

int main() {

	// change this to your directory
	std::string urdf_file_name = "/home/hsiuchin/code/COMP514/robots/ros_kortex/kortex_description/urdf/gen3.urdf" ;

	pinocchio::Model model;													// create a model object
	pinocchio::urdf::buildModel(urdf_file_name, model, false);				// read the URDF file
	pinocchio::Data data(model); 											// create the data structure for the calculations
	const int JOINT_ID = 7;

	int dim_joints = model.nq ;

	std::cout << "model.nq() = " << model.nq << std::endl ;

	// random reference pose
	Eigen::VectorXd joint_pos = Eigen::VectorXd::Random(dim_joints) ;
	Eigen::VectorXd joint_vel = Eigen::VectorXd::Random(dim_joints) ;
	std::cout << "joint_pos = " << joint_pos.transpose() << std::endl ;
	std::cout << "joint_vel = " << joint_vel.transpose() << std::endl ;
	pinocchio::forwardKinematics(model, data, joint_pos, joint_vel );							// forward kinematics
	pinocchio::SE3 pose_ref = data.oMi[JOINT_ID]  ;												// end-effector pose
	std::cout << "pose_ref =        \n" << pose_ref << std::endl ;

	// random feedback pose
	joint_pos = Eigen::VectorXd::Random(dim_joints) ;
	joint_vel = Eigen::VectorXd::Random(dim_joints) ;
	std::cout << "joint_pos = " << joint_pos.transpose() << std::endl ;
	std::cout << "joint_vel = " << joint_vel.transpose() << std::endl ;
	pinocchio::forwardKinematics(model, data, joint_pos, joint_vel );
	pinocchio::SE3 pose_fbk = data.oMi[JOINT_ID]  ;												// end-effector pose
	std::cout << "pose_fbk =        \n" << pose_fbk << std::endl ;


	difference_between_two_pose(pose_fbk, pose_ref) ;

	pinocchio_to_other_datatype(pose_fbk) ;


	return 0 ;
}
