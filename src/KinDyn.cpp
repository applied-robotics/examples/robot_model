#include "robot_model/KinDyn.hpp"

KinDyn::KinDyn(ros::NodeHandle& nodeHandle) : node_handle_(nodeHandle) {

	// read parameters
	if ( !readParameters() ) {
		ROS_ERROR("[KinDyn] Could not read parameters. Shut down");
		ros::requestShutdown();
	}
	init(urdf_file_name_) ;

	// initialize all eigen
	fbk_task_pos_ = Eigen::VectorXd::Zero(3) ;
	fbk_task_vel_ = Eigen::VectorXd::Zero(3) ;

}

void KinDyn::init(std::string urdf_file_name) {

	// build a pinocchio model
	pinocchio::urdf::buildModel(urdf_file_name_, model_, false) ;

	// data structure for pinocchio
	data_  		  = pinocchio::Data(model_) ;
	hand_id_ 	  = model_.getJointId("bracelet_link") - 1 ;

	dim_joints_   = model_.nq ;

	joint_pos_ 	  = Eigen::VectorXd::Zero(dim_joints_) ;
	joint_vel_ 	  = Eigen::VectorXd::Zero(dim_joints_) ;
	jacobian_	  = Eigen::MatrixXd::Zero(6, dim_joints_) ;
	jacobian_dot_ = Eigen::MatrixXd::Zero(6, dim_joints_) ;
	M_  		  = Eigen::MatrixXd::Identity(dim_joints_,dim_joints_) ;
	h_ 			  = Eigen::VectorXd::Zero(dim_joints_) ;
}

void KinDyn::update() {

	// assign some random position and velocity. this should be read from subscribers
	joint_pos_ = Eigen::VectorXd::Random(dim_joints_) ;
	joint_vel_ = Eigen::VectorXd::Random(dim_joints_) ;

	pinocchio::computeAllTerms(model_, data_, joint_pos_, joint_vel_) ; // compute all kinematics and dynamics related values
	pinocchio::getJointJacobian(model_, data_, hand_id_, pinocchio::ReferenceFrame::LOCAL_WORLD_ALIGNED, jacobian_) ; // get jacobian
	pinocchio::getJointJacobianTimeVariation(model_, data_, hand_id_, pinocchio::ReferenceFrame::LOCAL_WORLD_ALIGNED, jacobian_dot_) ; // time derivative of jacobian

	fbk_task_pos_ 	= data_.oMi[hand_id_].translation() ; // end-effector translation
	M_ 				= data_.M ; 	// mass matrix
	h_ 				= data_.nle ;	// coriolis, centrifugal, and gravity

	ROS_INFO_STREAM("joint_pos_ " << joint_pos_.transpose() ) ;
	ROS_INFO_STREAM("joint_vel_ " << joint_vel_.transpose() ) ;
	ROS_INFO_STREAM("M_ " << M_ ) ;
	ROS_INFO_STREAM("h_ " << h_.transpose() ) ;
	ROS_INFO_STREAM("jacobian_ " << jacobian_ ) ;
	ROS_INFO_STREAM("jacobian_dot_ " << jacobian_dot_ ) ;
}

bool KinDyn::readParameters() {

	// read all parameters here
	if ( !node_handle_.getParam("/gen3/urdf_file_name", urdf_file_name_) ) {
		ROS_ERROR_STREAM("cannot read /gen3/urdf_file_name") ;
		return false ;
	}

	return true ;
}
