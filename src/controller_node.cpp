#include "robot_model/KinDyn.hpp"


/********* VERY IMPORTANT: DO NOT PUT THIS LINE BEFORE PINOCCHIO ****************/
#include <ros/ros.h>



int main(int argc, char** argv) {

	ros::init(argc, argv, "controller") ;
	ros::NodeHandle node_handle("~") ;

	int publish_rate = 500 ;
	if ( !node_handle.getParam("/publish_rate", publish_rate) ) {
		ROS_ERROR("[controller_node] cannot read publish_rate") ;
		return false ;
	}
	else {
		ROS_DEBUG_STREAM ("[controller_node] publish_rate = " << publish_rate ) ;
	}

	ros::Rate rate(publish_rate) ;
	KinDyn model(node_handle);

	ROS_DEBUG_STREAM("[controller_node] node is ready") ;
	while ( ros::ok() ) {
		ros::spinOnce();
		model.update() ;
		rate.sleep();
	}


	return 0;
}
